export default function ({ store, redirect }) {
  if (store.getters['test/email'] === null) {
    redirect('/')
  }
}
