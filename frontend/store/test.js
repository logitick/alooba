export const state = () => ({
  email: null,
  submissionUrl: null,
  executeQueryUrl: null,
  startTime: null,
})

export const getters = {
  email: (state) => state.email,
}

export const mutations = {
  setEmail(state, email) {
    state.email = email
  },
  setSubmissionUrl(state, url) {
    state.submissionUrl = url
  },
  setExecuteQueryUrl(state, url) {
    state.executeQueryUrl = url
  },
  setStartTime(state, datetime) {
    state.startTime = datetime
  },
}

export const actions = {
  startTest({ commit }, email) {
    return this.$axios
      .$post('/api/test-response', { email })
      .then((response) => {
        commit('setEmail', response.email)
        commit('setSubmissionUrl', response.submission_url)
        commit('setExecuteQueryUrl', response.execute_query_url)
        commit('setStartTime', new Date())
        return response
      })
  },
  executeQuery({ commit, state }, query) {
    return new Promise((resolve, reject) => {
      this.$axios
        .$post(state.executeQueryUrl, {
          query,
        })
        .then((response) => {
          return resolve(response)
        })
        .catch((error) => {
          if (error.response.status === 400) {
            reject(error.response.data)
          }
          reject(error)
        })
    })
  },
  submitTest({ commit, state }, query) {
    // duration is in seconds
    const secondsDuration =
      (new Date().getTime() - state.startTime.getTime()) / 1000

    return new Promise((resolve, reject) => {
      this.$axios
        .$patch(state.submissionUrl, {
          seconds_duration: Math.round(secondsDuration),
          query,
        })
        .then(() => {
          // clear the data
          commit('setEmail', null)
          commit('setSubmissionUrl', null)
          commit('setExecuteQueryUrl', null)
          commit('setStartTime', null)
          resolve(true)
        })
        .catch((e) => reject(e))
    })
  },
}
