#!/usr/bin/env bash

docker-compose up -d
docker-compose exec api php artisan migrate --force
