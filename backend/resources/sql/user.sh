#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER test_submission_user with encrypted password 'Ea7S0Tik7kq85GXT';
    GRANT CONNECT ON DATABASE $POSTGRES_DB TO test_submission_user;
    GRANT USAGE ON SCHEMA public TO test_submission_user;
    GRANT SELECT ON google_users TO test_submission_user;
    GRANT SELECT ON users TO test_submission_user;
EOSQL
