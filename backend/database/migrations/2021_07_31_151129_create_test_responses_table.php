<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTestResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('test_responses', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('email');
            $table->text('query')->nullable(true);
            $table->boolean('query_output_correct')->nullable(true);
            $table->integer('seconds_duration')->nullable(true);
            $table->dateTimeTz('server_side_started');
            $table->dateTimeTz('server_side_ended')->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('test_responses');
    }
}
