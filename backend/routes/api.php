<?php

use App\Http\Controllers\ExecuteTestQueryController;
use App\Http\Controllers\TestResponseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('test-response', TestResponseController::class, ['only' => ['store', 'update']]);
Route::resource('execute-query', ExecuteTestQueryController::class, ['only' => ['store']])->middleware(['signed']);
