<?php

namespace App\Http\Controllers;

use App\Services\QueryEvaluator;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class ExecuteTestQueryController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, QueryEvaluator $queryEvaluator)
    {
        $responseData = [];
        $rawQuery = $request->input('query', '');

        try {
            $userQueryResult = $queryEvaluator->getResults($rawQuery);
            $responseData['isCorrect'] = $queryEvaluator->doesMatchExpectedResults($userQueryResult);
            $responseData['result'] = $userQueryResult;
        } catch (QueryException $e) {
            $responseData['sqlError'] = $e->errorInfo[2];
            $responseData['error'] = $e->getMessage();
            return response($responseData, 400);
        }
        return response($responseData);
    }

}
