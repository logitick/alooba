<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTestResponseRequest;
use App\Models\TestResponse;
use App\Services\QueryEvaluator;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class TestResponseController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTestResponseRequest $request)
    {
        $response = TestResponse::create([
            'email' => $request->input('email'),
            'server_side_started' => now()
        ]);

        return response(
            array_merge($response->toArray(),[
                'submission_url' => URL::signedRoute('test-response.update', [$response]),
                'execute_query_url' => URL::signedRoute('execute-query.store', [$response])
            ])
        );
    }

    public function update(TestResponse $testResponse, Request $request, QueryEvaluator $queryEvaluator)
    {
        if ($testResponse->server_side_ended !== null) {
            return abort(403);
        }
        try {
            $queryResults = $queryEvaluator->getResults($request->input('query', ''));
            $testResponse->query_output_correct = $queryEvaluator->doesMatchExpectedResults($queryResults);
        } catch (QueryException $exception) {
            $testResponse->query_output_correct = false;
        }
        $testResponse->query = $request->input('query', '');
        $testResponse->seconds_duration = $request->input('seconds_duration');
        $testResponse->server_side_ended = now();
        $testResponse->update();

        return response($testResponse);
    }
}
