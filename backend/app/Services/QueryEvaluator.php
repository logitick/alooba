<?php


namespace App\Services;


use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class QueryEvaluator
{
    public function doesMatchExpectedResults(Collection $userQueryResult): bool
    {
        $expectedResults = $this->getExpectedResults();
        return $expectedResults->pipe(function(Collection $expectedResults) use ($userQueryResult) {

            // length does not match
            if ($expectedResults->count() !== $userQueryResult->count()) {
                return false;
            }

            $match = false;
            $expectedResults->each(function($row, $key) use ($userQueryResult, &$match) {
                $match = $userQueryResult->get($key) == $row;
                return $match;
            });
            return $match;
        });
    }

    private function getExpectedResults(): Collection
    {
        return DB::connection('test-db')
            ->table('google_users')
            ->selectRaw('device_cat, COUNT(*)')
            ->join('users', 'google_users.user_id', '=', 'users.user_id')
            ->orderByDesc('count')
            ->whereBetween('users.last_login', ['2019-07-01', '2019-07-31 23:59:59'])
            ->groupBy('device_cat')
            ->get();
    }

    public function getResults(string $userQuery): Collection
    {
        return collect(DB::connection('test-db')
            ->select($userQuery));
    }

}
