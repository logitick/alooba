<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Class TestResponse
 * @package App\Models
 *
 * @property string id
 * @property string email
 * @property string|null query
 * @property boolean|null query_output_correct
 * @property int|null seconds_duration
 * @property Carbon server_side_started
 * @property Carbon|null server_side_ended
 */
class TestResponse extends Model
{
    public $incrementing = false;
    protected $keyType = 'string';
    public $timestamps = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function (Model $model) {
            $model->setAttribute($model->getKeyName(), Str::uuid());
        });
    }

    protected $fillable = [
        'email',
        'server_side_started'
    ];
}
